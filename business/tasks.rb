require 'lib/db'

module Business
  module Tasks
    def self.complete(rid)
      update = OrientDB::SQL::Update.new.oclass('Task')
        .fields(:status)
        .values('completed')
        .where(:@rid => rid)

      db.run_command(update)
    end

    def self.completed
      filter_tasks_by_status('completed')
    end

    def self.count_pending
      select = OrientDB::SQL::Query.new.from('Task').select('count(*)').where(:status => 'pending')
      db.first(select)[:count]
    end

    def self.create(params)
      task = OrientDB::Document.new db, 'Task',
        :title => params[:title],
        :project => OrientDB::CORE::id::ORecordId.new(params[:project_id]),
        :tags => (params[:tags] || []).map { |id| OrientDB::CORE::id::ORecordId.new(id) },
        :status => 'pending'

      task.save
    end

    def self.pending
      filter_tasks_by_status('pending')
    end

    def self.reject(rid)
      update = OrientDB::SQL::Update.new.oclass('Task')
        .fields(:status)
        .values('rejected')
        .where(:@rid => rid)

      db.run_command(update)
    end

    def self.rejected
      filter_tasks_by_status('rejected')
    end

    def self.filter_tasks_by_status(status)
      select = OrientDB::SQL::Query.new.from('Task').where(:status => status)
      tasks = db.all(select).to_a
    end

    def self.db
      DB.connection
    end
  end
end
