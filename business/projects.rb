require 'lib/db'

module Business
  module Projects
    def self.create(params)
      insert = OrientDB::SQL::Insert.new.oclass('Project')
        .fields(:name)
        .values(params[:name])

      db.run_command(insert)
    end

    def self.all
      select = OrientDB::SQL::Query.new.from('Project')
      db.all(select).to_a
    end

    def self.db
      DB.connection
    end
  end
end
