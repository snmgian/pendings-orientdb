require 'lib/db'

module Business
  module Tags
    def self.all
      select = OrientDB::SQL::Query.new.from('Tag')
      db.all(select).to_a
    end

    def self.db
      DB.connection
    end
  end
end
