require 'orientdb'
require 'sinatra/base'
require 'sinatra/partial'
require 'slim'

require 'business/projects'
require 'business/tags'
require 'business/tasks'

class Pendings < Sinatra::Base

  register Sinatra::Partial
  set :partial_template_engine, :slim

  before do
  end

  helpers do
    def rid_to_url(rid)
      rid.gsub('#', '').gsub(':', '_')
    end

    def url_to_rid(url)
      '#' << url.gsub('_', ':')
    end

    def cycle(*params)
      params[@_cycle = ((@_cycle || -1) + 1) % params.length]
    end
  end

  get '/' do
    @pending_tasks = Business::Tasks.pending
    @rejected_tasks = Business::Tasks.rejected
    @completed_tasks = Business::Tasks.completed
    @count_pending_tasks = Business::Tasks.count_pending

    slim :index
  end

  get '/projects' do
    @projects = Business::Projects.all
    slim :'projects/index'
  end

  post '/projects' do
    Business::Projects.create(params)
    redirect '/projects'
  end

  get '/projects/new' do
    slim :'projects/new'
  end

  post '/tasks' do
    Business::Tasks.create(params)
    redirect '/'
  end

  post '/tasks/:id/complete' do
    Business::Tasks.complete(url_to_rid(params[:id]))
    redirect '/'
  end

  post '/tasks/:id/reject' do
    Business::Tasks.reject(url_to_rid(params[:id]))
    redirect '/'
  end

  get '/tasks/new' do
    @projects = Business::Projects.all
    @tags = Business::Tags.all

    slim :'tasks/new'
  end

end
