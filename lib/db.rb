module DB

  def self.connection
    @db ||= OrientDB::DocumentDatabase.connect(
      Env.orientdb.url,
      Env.orientdb.user,
      Env.orientdb.pass
    )
  end

end
